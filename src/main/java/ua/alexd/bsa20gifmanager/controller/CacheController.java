package ua.alexd.bsa20gifmanager.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ua.alexd.bsa20gifmanager.dto.CatalogGifsDto;
import ua.alexd.bsa20gifmanager.dto.GifGenerateDto;
import ua.alexd.bsa20gifmanager.service.CacheService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/cache")
public class CacheController {

	private final CacheService cacheService;

	public CacheController(CacheService cacheService) {
		this.cacheService = cacheService;
	}

	@GetMapping
	public List<CatalogGifsDto> queryCacheGifs(@RequestHeader("X-BSA-GIPHY") String giphy,
	                                           @RequestParam String query) {
		return cacheService.queryGifs(query);
	}

	@PostMapping("/generate")
	public CatalogGifsDto generateCacheGif(@RequestHeader("X-BSA-GIPHY") String giphy,
	                                       @RequestBody @Valid GifGenerateDto generateDto) {
		return cacheService.generateGif(generateDto);
	}

	@DeleteMapping
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void deleteCacheGifs(@RequestHeader("X-BSA-GIPHY") String giphy) {
		cacheService.deleteGifs();
	}

}
