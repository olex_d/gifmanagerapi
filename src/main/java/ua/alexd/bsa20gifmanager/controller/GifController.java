package ua.alexd.bsa20gifmanager.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.alexd.bsa20gifmanager.service.GifService;

import java.util.List;

@RestController
@RequestMapping("/gifs")
public class GifController {

	private final GifService gifService;

	public GifController(GifService gifService) {
		this.gifService = gifService;
	}

	@GetMapping
	public List<String> getAllGifs(@RequestHeader("X-BSA-GIPHY") String giphy) {
		return gifService.getAll();
	}

}
