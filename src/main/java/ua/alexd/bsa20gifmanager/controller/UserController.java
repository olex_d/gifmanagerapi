package ua.alexd.bsa20gifmanager.controller;

import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ua.alexd.bsa20gifmanager.csv.CsvHistoryBean;
import ua.alexd.bsa20gifmanager.dto.CatalogGifsDto;
import ua.alexd.bsa20gifmanager.dto.GifGenerateDto;
import ua.alexd.bsa20gifmanager.service.UserService;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

@RestController
@RequestMapping("/user")
@Validated
public class UserController {

	private final UserService userService;

	public UserController(UserService userService) {
		this.userService = userService;
	}

	@GetMapping("/{id}/all")
	public List<CatalogGifsDto> getUserGifs(@RequestHeader("X-BSA-GIPHY") String giphy,
	                                        @PathVariable @NotBlank @Size(max = 10) String id) {
		return userService.queryAllUserGifs(id);
	}

	@GetMapping("/{id}/search")
	public String getUserHistory(@RequestHeader("X-BSA-GIPHY") String giphy,
	                             @PathVariable @NotBlank @Size(max = 10) String id,
	                             @RequestParam @NotBlank String query,
	                             @RequestParam(required = false, defaultValue = "false") Boolean force) {
		return userService.queryUserGifs(id, query, force);
	}

	@PostMapping("/{id}/generate")
	public String generateUserGif(@RequestHeader("X-BSA-GIPHY") String giphy,
	                              @PathVariable @NotBlank @Size(max = 10) String id,
	                              @RequestBody @Valid GifGenerateDto generateDto) {
		return userService.generateGif(id, generateDto);
	}

	@DeleteMapping("/{id}/clean")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void cleanUserData(@RequestHeader("X-BSA-GIPHY") String giphy,
	                          @PathVariable @NotBlank @Size(max = 10) String id) {
		userService.cleanUserData(id);
	}

	@DeleteMapping("/{id}/reset")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void resetUserMemoryCache(@RequestHeader("X-BSA-GIPHY") String giphy,
	                                 @PathVariable @NotBlank @Size(max = 10) String id,
	                                 @RequestParam String query) {
		userService.resetUserMemoryCache(id, query);
	}

	@GetMapping("/{id}/history")
	public List<CsvHistoryBean> getUserHistory(@RequestHeader("X-BSA-GIPHY") String giphy,
	                                           @PathVariable @NotBlank @Size(max = 10) String id) {
		return userService.getHistory(id);
	}

	@DeleteMapping("/{id}/history/clean")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void deleteUserHistory(@RequestHeader("X-BSA-GIPHY") String giphy,
	                              @PathVariable @NotBlank @Size(max = 10) String id) {
		userService.cleanHistory(id);
	}

}
