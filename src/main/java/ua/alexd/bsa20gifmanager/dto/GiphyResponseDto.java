package ua.alexd.bsa20gifmanager.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class GiphyResponseDto {
	private String id;
	private String url;
}
