package ua.alexd.bsa20gifmanager.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CatalogGifsDto {
	private String query;
	private List<String> gifs;
}
