package ua.alexd.bsa20gifmanager.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class GifGenerateDto {
	@NotBlank
	private String query;
	private Boolean force = false;
}
