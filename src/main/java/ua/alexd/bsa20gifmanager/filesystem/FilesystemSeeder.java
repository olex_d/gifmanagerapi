package ua.alexd.bsa20gifmanager.filesystem;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@Service
@Slf4j
public class FilesystemSeeder {

	@Value("${fs.cache}")
	private String cacheGifDir;
	@Value("${fs.users}")
	private String usersGifDir;

	@EventListener
	public void seed(ContextRefreshedEvent event) throws IOException {
		seedDir(cacheGifDir);
		seedDir(usersGifDir);
	}

	private void seedDir(String dir) throws IOException {
		var dirPath = Path.of(dir);
		if (!Files.exists(dirPath)) {
			var createdDir = Files.createDirectories(dirPath);
			log.info(createdDir.getFileName().toString() + " directory has been seeded");
		}
	}

}
