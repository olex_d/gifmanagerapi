package ua.alexd.bsa20gifmanager.filesystem;

import org.springframework.stereotype.Service;
import ua.alexd.bsa20gifmanager.dto.CatalogGifsDto;
import ua.alexd.bsa20gifmanager.formatting.ResponseFormatter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

@Service
public class FilesystemWalker {

	private final ResponseFormatter responseFormatter;
	private final Random rand;

	public FilesystemWalker(ResponseFormatter responseFormatter, Random rand) {
		this.responseFormatter = responseFormatter;
		this.rand = rand;
	}

	public Optional<String> getRandomCatalogGif(Path rootDir, String query) {
		var gifsCatalog = listCatalogsWithGifs(rootDir, query);
		if (gifsCatalog.isEmpty()) return Optional.empty();

		var gifs = gifsCatalog.get(0).getGifs();
		return Optional.of(
				gifs.get(rand.nextInt(gifs.size()))
		);
	}

	public List<CatalogGifsDto> listCatalogsWithGifs(Path rootDir) {
		return listCatalogsWithGifs(rootDir, "");
	}

	public List<CatalogGifsDto> listCatalogsWithGifs(Path rootDir, String query) {
		var catalogedGifs = new ArrayList<CatalogGifsDto>();

		var innerDirs = listDirs(rootDir, query);
		for (var dir : innerDirs) {
			var innerGifs = listGifs(dir);
			if (!innerGifs.isEmpty()) {
				var newCatalog = new CatalogGifsDto(
						responseFormatter.formatCatalogName(dir),
						responseFormatter.gifsPathsToUrl(innerGifs)
				);
				catalogedGifs.add(newCatalog);
			}
		}

		return catalogedGifs;
	}

	private List<Path> listDirs(Path rootDir, String query) {
		try (var walk = Files.walk(rootDir)) {
			return walk
					.filter(file -> {
						if (Files.isDirectory(file)) { // parse only directories
							if (file.equals(rootDir)) return false; // ignore root folder
							if (query.isBlank()) return true; // accept when query not specified
							else return query.equals(file.getFileName().toString()); // accept when query == filename
						}
						return false;
					})
					.collect(Collectors.toList());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new ArrayList<>();
	}

	public List<Path> listGifs(Path rootDir) {
		try (var walk = Files.walk(rootDir)) {
			return walk
					.map(Path::toString)
					.filter(f -> f.endsWith(".gif"))
					.map(Path::of)
					.collect(Collectors.toList());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new ArrayList<>();
	}

}
