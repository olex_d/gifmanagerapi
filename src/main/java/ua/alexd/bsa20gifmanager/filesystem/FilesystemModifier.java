package ua.alexd.bsa20gifmanager.filesystem;

import org.springframework.stereotype.Service;
import ua.alexd.bsa20gifmanager.csv.CsvHistoryIO;
import ua.alexd.bsa20gifmanager.dto.CatalogGifsDto;
import ua.alexd.bsa20gifmanager.dto.GifGenerateDto;
import ua.alexd.bsa20gifmanager.exception.type.DirectoryContendDeletionException;
import ua.alexd.bsa20gifmanager.formatting.ResponseFormatter;
import ua.alexd.bsa20gifmanager.giphy.GiphyDownloader;
import ua.alexd.bsa20gifmanager.memorycache.UsersCacheCoordinator;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;

@Service
public class FilesystemModifier {

	private final FilesystemWalker filesystemWalker;
	private final GiphyDownloader giphyDownloader;
	private final UsersCacheCoordinator cacheCoordinator;
	private final ResponseFormatter responseFormatter;
	private final CsvHistoryIO csvIO;

	public FilesystemModifier(FilesystemWalker filesystemWalker, GiphyDownloader giphyDownloader,
	                          UsersCacheCoordinator cacheCoordinator, ResponseFormatter responseFormatter,
	                          CsvHistoryIO csvIO) {
		this.filesystemWalker = filesystemWalker;
		this.giphyDownloader = giphyDownloader;
		this.cacheCoordinator = cacheCoordinator;
		this.responseFormatter = responseFormatter;
		this.csvIO = csvIO;
	}

	public CatalogGifsDto generateToCache(Path cacheGifDir, GifGenerateDto generateDto) {
		var queriedCatalog = filesystemWalker.listCatalogsWithGifs(
				cacheGifDir,
				generateDto.getQuery()
		);
		if (!queriedCatalog.isEmpty()) return queriedCatalog.get(0);

		giphyDownloader.downloadGif(cacheGifDir, generateDto);
		return generateToCache(cacheGifDir, generateDto); // traverse through filesystem again
	}

	public String generateToUser(String id, Path cacheDir, Path userDir, GifGenerateDto generateDto) throws IOException {
		if (!generateDto.getForce()) {
			var diskCacheGif = filesystemWalker.getRandomCatalogGif(cacheDir, generateDto.getQuery());
			if (diskCacheGif.isPresent()) {
				var gifPath = responseFormatter.gifUrlToPath(diskCacheGif.get());
				var userGifPath = userDir
						.resolve(generateDto.getQuery())
						.resolve(gifPath.getFileName());

				if (!Files.exists(userGifPath.getParent())) Files.createDirectories(userGifPath.getParent());
				if (!Files.exists(userGifPath)) {
					Files.copy(gifPath, userGifPath);
					cacheCoordinator.updateUserCacheFromDisk(id, userDir, generateDto.getQuery());
				}
				csvIO.updateUserHistory(userDir, generateDto.getQuery(), diskCacheGif.get());

				return diskCacheGif.get();
			}
		}

		giphyDownloader.downloadGif(cacheDir, generateDto);

		generateDto.setForce(false); // allow cache directory scanning
		return generateToUser(id, cacheDir, userDir, generateDto); // traverse through filesystem again
	}

	public void cleanDirectoryContent(Path rootDir) {
		try {
			Files.walk(rootDir)
					.sorted(Comparator.reverseOrder())
					.map(Path::toFile)
					.filter(file -> !file.toPath().equals(rootDir))
					.forEach(File::delete);
		} catch (IOException ex) {
			throw new DirectoryContendDeletionException();
		}
	}

}
