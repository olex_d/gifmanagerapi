package ua.alexd.bsa20gifmanager.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import ua.alexd.bsa20gifmanager.dto.CatalogGifsDto;
import ua.alexd.bsa20gifmanager.dto.GifGenerateDto;
import ua.alexd.bsa20gifmanager.exception.type.GifsNotFoundException;
import ua.alexd.bsa20gifmanager.filesystem.FilesystemModifier;
import ua.alexd.bsa20gifmanager.filesystem.FilesystemWalker;

import java.nio.file.Path;
import java.util.List;

@Service
public class CacheService {

	@Value("${fs.cache}")
	private String cacheDir;

	private final FilesystemWalker filesystemWalker;
	private final FilesystemModifier filesystemModifier;

	public CacheService(FilesystemWalker filesystemWalker, FilesystemModifier filesystemModifier) {
		this.filesystemWalker = filesystemWalker;
		this.filesystemModifier = filesystemModifier;
	}

	public List<CatalogGifsDto> queryGifs(String query) {
		var catalogs = filesystemWalker.listCatalogsWithGifs(Path.of(cacheDir), query);
		if (catalogs.isEmpty()) throw new GifsNotFoundException();

		return catalogs;
	}

	public CatalogGifsDto generateGif(@RequestBody GifGenerateDto generateDto) {
		return filesystemModifier.generateToCache(Path.of(cacheDir), generateDto);
	}

	public void deleteGifs() {
		filesystemModifier.cleanDirectoryContent(Path.of(cacheDir));
	}

}
