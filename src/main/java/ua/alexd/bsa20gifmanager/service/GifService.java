package ua.alexd.bsa20gifmanager.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ua.alexd.bsa20gifmanager.exception.type.GifsNotFoundException;
import ua.alexd.bsa20gifmanager.filesystem.FilesystemWalker;
import ua.alexd.bsa20gifmanager.formatting.ResponseFormatter;

import java.nio.file.Path;
import java.util.List;

@Service
public class GifService {

	@Value("${fs.cache}")
	private String cacheGifDir;

	private final FilesystemWalker filesystemWalker;
	private final ResponseFormatter responseFormatter;

	public GifService(FilesystemWalker filesystemWalker, ResponseFormatter responseFormatter) {
		this.filesystemWalker = filesystemWalker;
		this.responseFormatter = responseFormatter;
	}

	public List<String> getAll() {
		var gifsPaths = filesystemWalker.listGifs(Path.of(cacheGifDir));
		if (gifsPaths.isEmpty()) throw new GifsNotFoundException();

		return responseFormatter.gifsPathsToUrl(gifsPaths);
	}
}
