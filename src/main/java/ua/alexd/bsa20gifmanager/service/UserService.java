package ua.alexd.bsa20gifmanager.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ua.alexd.bsa20gifmanager.csv.CsvHistoryBean;
import ua.alexd.bsa20gifmanager.csv.CsvHistoryIO;
import ua.alexd.bsa20gifmanager.dto.CatalogGifsDto;
import ua.alexd.bsa20gifmanager.dto.GifGenerateDto;
import ua.alexd.bsa20gifmanager.exception.type.GifGenerationException;
import ua.alexd.bsa20gifmanager.exception.type.GifsNotFoundException;
import ua.alexd.bsa20gifmanager.exception.type.UserNotExistException;
import ua.alexd.bsa20gifmanager.filesystem.FilesystemModifier;
import ua.alexd.bsa20gifmanager.filesystem.FilesystemWalker;
import ua.alexd.bsa20gifmanager.formatting.UserFormatter;
import ua.alexd.bsa20gifmanager.memorycache.UsersCacheCoordinator;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

@Service
public class UserService {

	@Value("${fs.cache}")
	private String cacheDir;
	@Value("${fs.users}")
	private String usersDir;

	private final FilesystemWalker filesystemWalker;
	private final FilesystemModifier filesystemModifier;
	private final UserFormatter userFormatter;
	private final UsersCacheCoordinator cacheCoordinator;
	private final CsvHistoryIO csvIO;

	public UserService(FilesystemWalker filesystemWalker, FilesystemModifier filesystemModifier,
	                   UserFormatter userFormatter, UsersCacheCoordinator cacheCoordinator, CsvHistoryIO csvIO) {
		this.filesystemWalker = filesystemWalker;
		this.filesystemModifier = filesystemModifier;
		this.userFormatter = userFormatter;
		this.cacheCoordinator = cacheCoordinator;
		this.csvIO = csvIO;
	}

	public List<CatalogGifsDto> queryAllUserGifs(String id) {
		var generalizedId = userFormatter.formatUserId(id);
		var userPath = getUserPath(generalizedId);

		return filesystemWalker.listCatalogsWithGifs(userPath);
	}

	public String queryUserGifs(String id, String query, Boolean force) {
		var generalizedId = userFormatter.formatUserId(id);
		var userPath = getUserPath(generalizedId);

		if (!force) {
			var memoryCacheGif = cacheCoordinator.getRandomUserGif(generalizedId, query);
			if (memoryCacheGif.isPresent()) return memoryCacheGif.get();
		}

		var randomDiskGif = filesystemWalker.getRandomCatalogGif(userPath, query);
		if (randomDiskGif.isEmpty()) throw new GifsNotFoundException();

		cacheCoordinator.updateUserCacheFromDisk(generalizedId, userPath, query);

		return randomDiskGif.get();
	}

	public String generateGif(String id, GifGenerateDto generateDto) {
		try {
			var generalizedId = userFormatter.formatUserId(id);
			var userPath = createUserPath(generalizedId);

			return filesystemModifier.generateToUser(generalizedId, Path.of(cacheDir), userPath, generateDto);
		} catch (IOException ex) {
			throw new GifGenerationException();
		}
	}

	public void resetUserMemoryCache(String id) {
		resetUserMemoryCache(id, "");
	}

	public void resetUserMemoryCache(String id, String query) {
		var generalizedId = userFormatter.formatUserId(id);
		getUserPath(generalizedId);
		cacheCoordinator.resetUserCache(generalizedId, query);
	}

	public void cleanUserData(String id) {
		var generalizedId = userFormatter.formatUserId(id);
		var userPath = getUserPath(generalizedId);

		resetUserMemoryCache(generalizedId);
		filesystemModifier.cleanDirectoryContent(userPath);
	}

	public List<CsvHistoryBean> getHistory(String id) {
		var generalizedId = userFormatter.formatUserId(id);
		var userPath = getUserPath(generalizedId);
		return csvIO.readUserHistory(userPath);
	}

	public void cleanHistory(String id) {
		var generalizedId = userFormatter.formatUserId(id);
		var userPath = getUserPath(generalizedId);
		csvIO.cleanHistory(userPath);
	}

	private Path getUserPath(String id) {
		var userPath = Path.of(usersDir, id);
		if (!Files.exists(userPath))
			throw new UserNotExistException("User '" + id + "' hasn't been found in the filesystem");
		return userPath;
	}

	private Path createUserPath(String id) throws IOException {
		var userPath = Path.of(usersDir, id);
		if (!Files.exists(userPath)) Files.createDirectories(userPath);
		return userPath;
	}

}
