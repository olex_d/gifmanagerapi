package ua.alexd.bsa20gifmanager.formatting;

import org.springframework.stereotype.Service;

import java.util.regex.Pattern;

@Service
public class GiphyUrlFormatter {

	// convert giphy site to downloadable link (article: https://bit.ly/2YYpIe6)
	public String formatGiphyUrlToGifUrl(String giphyUrl) {
		final var giphySitePattern = Pattern.compile("media[0-9]+");
		return giphySitePattern.matcher(giphyUrl).replaceAll("i");
	}

}
