package ua.alexd.bsa20gifmanager.formatting;

import org.springframework.stereotype.Service;

@Service
public class UserFormatter {

	public String formatUserId(String id) {
		return id.trim().toLowerCase();
	}

}
