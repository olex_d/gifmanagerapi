package ua.alexd.bsa20gifmanager.formatting;

import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ResponseFormatter {

	public String formatCatalogName(Path catalogDir) {
		return catalogDir.getFileName().toString();
	}

	public List<String> gifsPathsToUrl(List<Path> gifsPaths) {
		return gifsPaths
				.stream()
				.map(file -> getCurrentUrl() + file.toString()
							.replace("\\", "/"))
				.collect(Collectors.toList());
	}

	public Path gifUrlToPath(String gifUrl) {
		var gifPath = gifUrl.replace(getCurrentUrl(), "");
		return Path.of(gifPath);
	}

	private String getCurrentUrl() {
		return ServletUriComponentsBuilder.fromCurrentContextPath().toUriString() + '/';
	}

}
