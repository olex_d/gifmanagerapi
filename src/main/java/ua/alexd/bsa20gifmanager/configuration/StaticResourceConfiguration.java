package ua.alexd.bsa20gifmanager.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.nio.file.Path;

@Configuration
public class StaticResourceConfiguration implements WebMvcConfigurer {

	@Value("${fs.base}")
	private String gifsFolder;

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		var baseFolderPath = Path.of(gifsFolder).toAbsolutePath();
		var sharedFolderPath = "file:///" + baseFolderPath + "\\";

		registry
				.addResourceHandler("/bsa_giphy/**")
				.addResourceLocations(sharedFolderPath);
	}

}
