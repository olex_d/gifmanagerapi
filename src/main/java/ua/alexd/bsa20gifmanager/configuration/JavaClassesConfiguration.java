package ua.alexd.bsa20gifmanager.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.net.http.HttpClient;
import java.util.Random;

@Configuration
public class JavaClassesConfiguration {

	@Bean
	@Scope(value = "prototype")
	public HttpClient httpClient() {
		return HttpClient.newHttpClient();
	}

	@Bean
	@Scope(value = "prototype")
	public Random random() {
		return new Random();
	}

}
