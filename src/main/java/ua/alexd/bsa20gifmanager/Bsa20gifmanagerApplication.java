package ua.alexd.bsa20gifmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@PropertySource({"classpath:${envTarget}.properties"})
public class Bsa20gifmanagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(Bsa20gifmanagerApplication.class, args);
	}

}
