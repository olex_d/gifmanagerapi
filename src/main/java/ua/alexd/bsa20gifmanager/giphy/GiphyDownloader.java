package ua.alexd.bsa20gifmanager.giphy;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ua.alexd.bsa20gifmanager.dto.GifGenerateDto;
import ua.alexd.bsa20gifmanager.dto.GiphyResponseDto;
import ua.alexd.bsa20gifmanager.exception.type.GiphyNotFoundException;
import ua.alexd.bsa20gifmanager.exception.type.GiphySearchExecutionException;
import ua.alexd.bsa20gifmanager.formatting.GiphyUrlFormatter;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.channels.Channels;
import java.nio.file.Files;
import java.nio.file.Path;

@Service
public class GiphyDownloader {

	@Value("${giphy.base_search_url}")
	private String GIPHY_URL;

	@Value("${giphy.secret}")
	private String GIPHY_KEY;

	private final HttpClient client;
	private final GiphyUrlFormatter giphyUrlFormatter;

	public GiphyDownloader(HttpClient httpClient, GiphyUrlFormatter giphyUrlFormatter) {
		this.client = httpClient;
		this.giphyUrlFormatter = giphyUrlFormatter;
	}

	public void downloadGif(Path rootDir, GifGenerateDto generateDto) {
		var searchUri = createSearchUri(generateDto);
		var downloadGif = getApiGif(searchUri);
		saveToDir(rootDir, generateDto, downloadGif);
	}

	private URI createSearchUri(GifGenerateDto generateDto) {
		try {
			var fullUrl = GIPHY_URL +
					"?api_key=" + GIPHY_KEY +
					"&tag=" + generateDto.getQuery();
			return new URI(fullUrl);
		} catch (URISyntaxException ex) {
			throw new GiphySearchExecutionException();
		}
	}

	private GiphyResponseDto getApiGif(URI searchUri) {
		try {
			var request = HttpRequest.newBuilder().uri(searchUri).build();
			var response = client.send(request, HttpResponse.BodyHandlers.ofString());

			var responseData = new JSONObject(response.body());
			var gif = responseData.getJSONObject("data");
			if (gif.isEmpty()) throw new GiphyNotFoundException();

			var giphyUrl = gif.getJSONObject("images").getJSONObject("downsized").getString("url");
			var downloadableUrl = giphyUrlFormatter.formatGiphyUrlToGifUrl(giphyUrl);

			return new GiphyResponseDto(
					gif.getString("id"),
					downloadableUrl
			);
		} catch (IOException | InterruptedException ex) {
			throw new GiphySearchExecutionException();
		}
	}

	// download and save to the new catalog directory
	private void saveToDir(Path rootDir, GifGenerateDto generateDto, GiphyResponseDto downloadGif) {
		try {
			var newGifPath = rootDir
					.resolve(generateDto.getQuery())
					.resolve(downloadGif.getId() + ".gif");

			if (Files.exists(newGifPath)) return; // forbid duplicates

			var catalogPath = newGifPath.getParent();
			if (!Files.exists(catalogPath)) Files.createDirectories(catalogPath);

			var gifUrlStream = new URL(downloadGif.getUrl()).openStream();
			var byteChannel = Channels.newChannel(gifUrlStream);
			var fileOutputStream = new FileOutputStream(newGifPath.toString());
			fileOutputStream.getChannel().transferFrom(byteChannel, 0, Long.MAX_VALUE);

			byteChannel.close();
			fileOutputStream.close();
		} catch (IOException ex) {
			throw new GiphySearchExecutionException();
		}
	}

}
