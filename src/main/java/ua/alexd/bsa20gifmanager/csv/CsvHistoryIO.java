package ua.alexd.bsa20gifmanager.csv;

import com.opencsv.CSVWriter;
import com.opencsv.bean.CsvToBeanBuilder;
import org.springframework.stereotype.Service;
import ua.alexd.bsa20gifmanager.exception.type.HistoryCsvException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.time.LocalDate;
import java.util.List;

@Service
public class CsvHistoryIO {

	@SuppressWarnings({"rawtypes", "unchecked"})
	public List<CsvHistoryBean> readUserHistory(Path userDir) {
		var userHistoryPath = getUserHistoryPath(userDir);

		try (var reader = Files.newBufferedReader(userHistoryPath)) {
			return new CsvToBeanBuilder(reader)
					.withType(CsvHistoryBean.class)
					.withIgnoreLeadingWhiteSpace(true)
					.build().parse();
		} catch (IOException exception) {
			throw new HistoryCsvException("Cannot perform reading of user history csv file");
		}
	}

	public void updateUserHistory(Path userDir, String query, String gifUrl) {
		var userHistoryPath = getUserHistoryPath(userDir);

		try (
				var writer = Files.newBufferedWriter(userHistoryPath,
						StandardOpenOption.CREATE, StandardOpenOption.APPEND);

				var csvWriter = new CSVWriter(writer,
						CSVWriter.DEFAULT_SEPARATOR,
						CSVWriter.NO_QUOTE_CHARACTER,
						CSVWriter.DEFAULT_ESCAPE_CHARACTER,
						CSVWriter.RFC4180_LINE_END);
		) {
			var currentDate = LocalDate.now().toString();
			var historyRecord = new String[]{currentDate, query, gifUrl};

			csvWriter.writeNext(historyRecord);
		} catch (IOException ioException) {
			throw new HistoryCsvException("Cannot perform updating of user history csv file");
		}
	}

	public void cleanHistory(Path userDir) {
		try {
			var userHistoryPath = getUserHistoryPath(userDir);
			Files.deleteIfExists(userHistoryPath);
		} catch (IOException ioException) {
			throw new HistoryCsvException("Cannot perform cleaning of user history csv file");
		}
	}

	private Path getUserHistoryPath(Path userDir) {
		return userDir.resolve("history.csv");
	}

}
