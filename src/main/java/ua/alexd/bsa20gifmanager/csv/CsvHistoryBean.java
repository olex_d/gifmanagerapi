package ua.alexd.bsa20gifmanager.csv;

import com.opencsv.bean.CsvBindByPosition;
import com.opencsv.bean.CsvDate;
import lombok.Data;

import java.time.LocalDate;

@Data
public class CsvHistoryBean {

	@CsvDate(value = "yyyy-MM-dd")
	@CsvBindByPosition(position = 0)
	private LocalDate date;

	@CsvBindByPosition(position = 1)
	private String query;

	@CsvBindByPosition(position = 2)
	private String gif;

}
