package ua.alexd.bsa20gifmanager.exception.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import ua.alexd.bsa20gifmanager.exception.type.GifGenerationException;
import ua.alexd.bsa20gifmanager.exception.type.GiphyNotFoundException;
import ua.alexd.bsa20gifmanager.exception.type.GiphySearchExecutionException;

@ControllerAdvice
public class GiphyHandler extends ResponseEntityExceptionHandler {

	private final HandlerResponseBuilder responseBuilder;

	public GiphyHandler(HandlerResponseBuilder responseBuilder) {
		this.responseBuilder = responseBuilder;
	}

	@ExceptionHandler({GiphySearchExecutionException.class})
	public ResponseEntity<Object> handleSearchExecutionException(GiphySearchExecutionException ex) {
		final var defaultExceptionMessage = "Service cannot receive searching data from Giphy";
		return responseBuilder.createErrorResponse(defaultExceptionMessage, HttpStatus.UNPROCESSABLE_ENTITY, ex);
	}

	@ExceptionHandler({GifGenerationException.class})
	public ResponseEntity<Object> handleGenerationException(GifGenerationException ex) {
		final var defaultExceptionMessage = "Server cannot process user generation request";
		return responseBuilder.createErrorResponse(defaultExceptionMessage, HttpStatus.UNPROCESSABLE_ENTITY, ex);
	}

	@ExceptionHandler({GiphyNotFoundException.class})
	public ResponseEntity<Object> handleGiphyNotFoundException(GiphyNotFoundException ex) {
		final var defaultExceptionMessage = "Giphy cannot find gif by specified query";
		return responseBuilder.createErrorResponse(defaultExceptionMessage, HttpStatus.NOT_FOUND, ex);
	}

}
