package ua.alexd.bsa20gifmanager.exception.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
public class HandlerResponseBuilder {

	public ResponseEntity<Object> createErrorResponse(String defaultMessage, HttpStatus status, Exception ex) {
		return ResponseEntity
				.status(status)
				.body(Map.of("error", Objects.isNull(ex.getMessage()) ? defaultMessage : ex.getMessage()));
	}

	public ResponseEntity<Object> createErrorsResponse(HttpStatus status, List<String> errors) {
		return ResponseEntity
				.status(status)
				.body(Map.of("errors", errors));
	}

}
