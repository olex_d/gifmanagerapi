package ua.alexd.bsa20gifmanager.exception.handler;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.stream.Collectors;

@ControllerAdvice
public class ControllerHandler extends ResponseEntityExceptionHandler {

	private final HandlerResponseBuilder responseBuilder;

	public ControllerHandler(HandlerResponseBuilder responseBuilder) {
		this.responseBuilder = responseBuilder;
	}

	@Override
	protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex,
	                                                                      HttpHeaders headers, HttpStatus status,
	                                                                      WebRequest request) {
		final var defaultExceptionMessage = "Request missing one or more required parameters";
		return responseBuilder.createErrorResponse(defaultExceptionMessage, HttpStatus.BAD_REQUEST, ex);
	}

	@ExceptionHandler({MissingRequestHeaderException.class})
	public ResponseEntity<Object> handleMissingHeaderException(MissingRequestHeaderException ex) {
		final var defaultExceptionMessage = "Request doesn't contain one or more required headers";
		return responseBuilder.createErrorResponse(defaultExceptionMessage, HttpStatus.FORBIDDEN, ex);
	}

	@ExceptionHandler({ConstraintViolationException.class})
	public ResponseEntity<Object> handleValidationException(ConstraintViolationException ex) {
		final var defaultExceptionMessage = "Request parameter doesn't match constraints";
		return responseBuilder.createErrorResponse(defaultExceptionMessage, HttpStatus.BAD_REQUEST, ex);
	}

	@ExceptionHandler({MethodArgumentTypeMismatchException.class})
	public ResponseEntity<Object> handleArgumentTypeMismatchException(MethodArgumentTypeMismatchException ex) {
		final var defaultExceptionMessage = "Request parameter doesn't match required type";
		return responseBuilder.createErrorResponse(defaultExceptionMessage, HttpStatus.BAD_REQUEST, ex);
	}

	@Override
	public ResponseEntity<Object> handleMethodArgumentNotValid(
			final MethodArgumentNotValidException ex,
			final HttpHeaders headers,
			final HttpStatus status,
			final WebRequest request) {
		var errors = new ArrayList<String>();

		errors.addAll(ex.getBindingResult().getFieldErrors()
				.stream()
				.map(error -> error.getField() + ": " + error.getDefaultMessage())
				.collect(Collectors.toList()));

		errors.addAll(ex.getBindingResult().getGlobalErrors()
				.stream()
				.map(error -> error.getObjectName() + ": " + error.getDefaultMessage())
				.collect(Collectors.toList()));

		return responseBuilder.createErrorsResponse(HttpStatus.BAD_REQUEST, errors);
	}

}
