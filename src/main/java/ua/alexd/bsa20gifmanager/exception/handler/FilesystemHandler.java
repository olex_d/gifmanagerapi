package ua.alexd.bsa20gifmanager.exception.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import ua.alexd.bsa20gifmanager.exception.type.DirectoryContendDeletionException;
import ua.alexd.bsa20gifmanager.exception.type.GifsNotFoundException;
import ua.alexd.bsa20gifmanager.exception.type.HistoryCsvException;
import ua.alexd.bsa20gifmanager.exception.type.UserNotExistException;

@ControllerAdvice
public class FilesystemHandler extends ResponseEntityExceptionHandler {

	private final HandlerResponseBuilder responseBuilder;

	public FilesystemHandler(HandlerResponseBuilder responseBuilder) {
		this.responseBuilder = responseBuilder;
	}

	@ExceptionHandler({GifsNotFoundException.class})
	public ResponseEntity<Object> handleGifsNotFoundException(GifsNotFoundException ex) {
		final var defaultExceptionMessage = "Gifs are not presented with provided query";
		return responseBuilder.createErrorResponse(defaultExceptionMessage, HttpStatus.NOT_FOUND, ex);
	}

	@ExceptionHandler({UserNotExistException.class})
	public ResponseEntity<Object> handleDirectoryContendDeletionException(UserNotExistException ex) {
		final var defaultExceptionMessage = "User with provided id hasn't been found in the filesystem";
		return responseBuilder.createErrorResponse(defaultExceptionMessage, HttpStatus.NOT_FOUND, ex);
	}

	@ExceptionHandler({DirectoryContendDeletionException.class})
	public ResponseEntity<Object> handleDirectoryContendDeletionException(DirectoryContendDeletionException ex) {
		final var defaultExceptionMessage = "Cannot perform deletion of content in directory";
		return responseBuilder.createErrorResponse(defaultExceptionMessage, HttpStatus.BAD_REQUEST, ex);
	}

	@ExceptionHandler({HistoryCsvException.class})
	public ResponseEntity<Object> handleHistoryCsvException(HistoryCsvException ex) {
		final var defaultExceptionMessage = "Unprocessable user history csv file";
		return responseBuilder.createErrorResponse(defaultExceptionMessage, HttpStatus.UNPROCESSABLE_ENTITY, ex);
	}

}
