package ua.alexd.bsa20gifmanager.exception.type;

public class DirectoryContendDeletionException extends RuntimeException {

	public DirectoryContendDeletionException() {
		super();
	}

	public DirectoryContendDeletionException(final String message) {
		super(message);
	}

}
