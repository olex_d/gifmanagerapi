package ua.alexd.bsa20gifmanager.exception.type;

public class HistoryCsvException extends RuntimeException {

	public HistoryCsvException() {
		super();
	}

	public HistoryCsvException(final String message) {
		super(message);
	}

}
