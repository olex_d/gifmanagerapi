package ua.alexd.bsa20gifmanager.exception.type;

public class GiphyNotFoundException extends RuntimeException {

	public GiphyNotFoundException() {
		super();
	}

	public GiphyNotFoundException(final String message) {
		super(message);
	}

}
