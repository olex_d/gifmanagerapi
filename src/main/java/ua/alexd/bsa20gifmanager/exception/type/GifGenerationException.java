package ua.alexd.bsa20gifmanager.exception.type;

public class GifGenerationException extends RuntimeException {

	public GifGenerationException() {
		super();
	}

	public GifGenerationException(final String message) {
		super(message);
	}

}
