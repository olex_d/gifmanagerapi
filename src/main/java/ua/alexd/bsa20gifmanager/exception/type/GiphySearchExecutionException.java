package ua.alexd.bsa20gifmanager.exception.type;

public class GiphySearchExecutionException extends RuntimeException {

	public GiphySearchExecutionException() {
		super();
	}

	public GiphySearchExecutionException(final String message) {
		super(message);
	}

}
