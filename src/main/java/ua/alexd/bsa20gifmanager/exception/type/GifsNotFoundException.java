package ua.alexd.bsa20gifmanager.exception.type;

public class GifsNotFoundException extends RuntimeException {

	public GifsNotFoundException() {
		super();
	}

	public GifsNotFoundException(final String message) {
		super(message);
	}

}
