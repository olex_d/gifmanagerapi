package ua.alexd.bsa20gifmanager.exception.type;

public class UserNotExistException extends RuntimeException {

	public UserNotExistException() {
		super();
	}

	public UserNotExistException(final String message) {
		super(message);
	}

}
