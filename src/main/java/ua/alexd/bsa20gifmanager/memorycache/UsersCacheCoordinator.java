package ua.alexd.bsa20gifmanager.memorycache;

import org.springframework.stereotype.Component;
import ua.alexd.bsa20gifmanager.dto.CatalogGifsDto;
import ua.alexd.bsa20gifmanager.filesystem.FilesystemWalker;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;

@Component
public class UsersCacheCoordinator {

	private final Map<String, List<CatalogGifsDto>> usersCache = new HashMap<>(); // users memory cache

	private final FilesystemWalker filesystemWalker;
	private final Random rand;

	public UsersCacheCoordinator(FilesystemWalker filesystemWalker, Random rand) {
		this.filesystemWalker = filesystemWalker;
		this.rand = rand;
	}

	public List<CatalogGifsDto> getUserGifsCatalogs(String id) {
		return usersCache.get(id);
	}

	public Optional<String> getRandomUserGif(String id, String query) {
		var userCatalog = getUserGifsCatalog(id, query);
		if (userCatalog.isEmpty()) return Optional.empty();

		var catalogGifs = userCatalog.get().getGifs();
		if (catalogGifs.isEmpty()) return Optional.empty();

		return Optional.of(catalogGifs.get(rand.nextInt(catalogGifs.size())));
	}

	public Optional<CatalogGifsDto> getUserGifsCatalog(String id, String query) {
		var userCatalogs = getUserGifsCatalogs(id);
		return Objects.isNull(userCatalogs)
				? Optional.empty()
				: userCatalogs
					.stream()
					.filter(user -> query.equals(user.getQuery()))
					.findAny();
	}

	public void updateUserCacheFromDisk(String id, Path userDir, String query) {
		var diskCatalogList = filesystemWalker.listCatalogsWithGifs(userDir, query);
		if (diskCatalogList.isEmpty()) return;
		var diskCatalog = diskCatalogList.get(0);

		if (usersCache.containsKey(id)) { // update existing catalog
			resetUserCache(id, query);
			usersCache.get(id).add(diskCatalog);
		} else {
			usersCache.put(id, new ArrayList<>(List.of(diskCatalog)));
		}
	}

	public void resetUserCache(String id, String query) {
		if (query.isBlank()) usersCache.remove(id);
		else {
			var userCatalogs = getUserGifsCatalog(id, query);
			userCatalogs.ifPresent(catalog -> usersCache.get(id).remove(catalog));
		}
	}

}
