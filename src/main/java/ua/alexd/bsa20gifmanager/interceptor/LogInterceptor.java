package ua.alexd.bsa20gifmanager.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;

@Slf4j
public class LogInterceptor extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
	                         Object handler) throws Exception {
		var queryString = Objects.nonNull(request.getQueryString())
				? '?' + request.getQueryString()
				: "";

		var logMessage = "User request:: " +
				"Method: '" + request.getMethod() +
				"', URL: '" + request.getRequestURL() + queryString;
		log.info(logMessage);

		return super.preHandle(request, response, handler);
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response,
	                            Object handler, Exception ex) throws Exception {
		var logMessage = "User answer:: Status:" + response.getStatus();
		log.info(logMessage);

		super.afterCompletion(request, response, handler, ex);
	}
}
